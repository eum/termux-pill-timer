# termux-pill-timer
[_English read-me below_](https://codeberg.org/eum/termux-pill-timer/src/branch/main/README.md#english-read-me)

Compte-à-rebours pour aider à prendre ses médicaments à la distance appropriée après et avant les repas. 

Il peut être utilisé dans Termux sur Android et éventuellement lancé à partir de l'écran d'accueil, par l'outil Termux:Widget.*

# En bref

Par défaut, les temps d'attente sont 2 heures après le repas et avant la prise de médicament, puis 1 heure de plus avant de pouvoir manger à nouveau. Lorsque l'on doit prendre ses médicaments, le script avertit en jouant un fichier de musique. 
Lorsque l'on peut manger à nouveau, une notification simple sonnera, ou au moins sera affichée.

# Configuration

- Installer les applis [Termux](https://f-droid.org/fr/packages/com.termux) et [Termux:Widget](https://f-droid.org/fr/packages/com.termux.widget), par exemple avec la boutique d'applications [F-Droid](http://f-droid.org/f-droid.apk).


- Depuis Termux, installer mplayer avec commande
```
pkg install mplayer
```

- Télécharger [un modèle](https://codeberg.org/eum/termux-pill-timer/raw/branch/main/termux-pill-timer-template) se l'adapter :
Sélectionner le fichier de musique à jouer pour prévenir de la prise de médicaments, et par défaut il n'y aura qu'une notification système classique lorsque le 2e timer (après la prise de médicament) se termine.
Définir les durées de la 1ère et 2e minuterie, généralement 2h et 1h respectivement (par défaut).
Déplacer le modèle maintenant configuré dans 

``` ~/.termux/widget/dynamic_shortcuts/ ``` 

"~" étant le dossier _home_ de Termux, celui dans lequel on se trouve au début.

Et le renommer comme vous voulez

Ajouter l'applet Termux:widget à votre écran d'accueil

# Utilisation

Lancez le script depuis le widget
Laissez le minuteur courir, passer à d'autres applications, sans fermer le termeux qui s'ouvre (vous pouvez revenir à lui pour vérifier combien de temps reste)
Lorsque la musique sonne, appuyez sur Ctrl + C ou Q dans mplayer pour quitter et poursuivre le script
Il vous demandera de prendre votre pilule, et de taper "y" quand vous avez fait
Le 2ème minuteur commence alors, et vous enverra une notification quand il est fini et vous pourrez manger à nouveau

# English Read-me

Timer to help you take your pill at the adequate distance after and before meals.

It can be used in Termux on Android and eventually launched from the home screen, by the Termux:Widget tool.

#  More..
By default the times to wait are 2 hours after meal and before medecine take, and then 1 more hour before eating again.
When you are supposed to take the medecine, the script warns you by playing a music file. 
When you can eat again, a simple notification will ring, or at least be displayed.

# Configuration
- Install [Termux](https://f-droid.org/fr/packages/com.termux) and[Termux:Widget](https://f-droid.org/fr/packages/com.termux.widget) apps, for example from [F-Droid](http://f-droid.org/f-droid.apk) app store.
- From Termux install mplayer with command
```
pkg install mplayer 
```
- Download a [template](https://codeberg.org/eum/termux-pill-timer/raw/branch/main/termux-pill-timer-template) and adapt it to your needs :
 - Select the music file to be played when you must take your medecine (after 1st timer), and by default there will only be a classic system notification when the 2nd timer (after the medecine take) ends.
 - Set the 1st and 2nd timer durations, usually 2h and 1h respecticely (default setting).
- Move your now configured template in ~/.termux/widget/dynamic_shortcuts/
"~" being Termux's own home folder, the one it starts in.
- and rename it the way you want
- Add the Termux:Widget widget to your home screen

# Usage
- Launch the script from the home widget
- Let the timer run, switch to other apps, without closing the termux that just openef
(you can come back to it to check how much time is left)
- When the music rings, press Ctrl + C or Q in mplayer to quit and continue the script
- It will ask you to take your pill, and type "y" when you did
- The 2nd timer then starts, and will send you a notification when it is over and you can eat again